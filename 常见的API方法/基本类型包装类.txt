为什么会有基本类型包装类

为了对基本数据类型进行更多的操作,更方便的操作,java就针对每一种基本数据类型提供了对应的类类型.
常用操作:博客
常用的操作之一：用于基本数据类型与字符串之间的转换

基本类型和包装类的对应
byte           Byte
short          Short
int            Integer
long           Long
float          Float
double         Double
char           Character
boolean        Boolean
