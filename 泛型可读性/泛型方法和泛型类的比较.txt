public class A<T>(){
    //泛型类的成员方法，该T受A后面的T的限制
    public T memberFunc(){
        return null;
    }
    //泛型方法，这里的T和和类A的T是不同的
    public static <T> T genericFunc(T a){
        return null;
    }
    public static void main(String[] args) {
        //编译不通过
        //Integer i = A<String>().findByUserName("s");
        
        //编译通过
        Set<Integer> set=  A<String>().findByConditions("s");
    }
}
这里Integer i = A<String>().findByUserName("s");会编译报错：
Error:(35, 61) java: 不兼容的类型: java.lang.String无法转换为java.lang.Integer`
由这个例子可知，泛型方法的T和和类A的T是不同的。